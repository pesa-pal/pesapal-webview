package com.example.pesapal_bills

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


open class MainActivity : AppCompatActivity() {
    private var TAG="MainActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webView.webViewClient = WebViewClient()

        webView.loadUrl("https://www.pesapal.com/bills")

        val webSettings = webView.settings
        webSettings .javaScriptEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportZoom(true)

        webView.webViewClient = object:WebViewClient(){

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                Log.i(TAG,"page loaded")

                if (view != null) {
                    view.loadUrl("javascript:(function() { " +
                            "document.querySelector('.slidearea').style.display='none'; " +
                            "})()")
                    view.loadUrl("javascript:(function() { " +
                            "document.querySelector('.expl').style.display='none'; " +
                            "})()")

                    view.loadUrl("javascript:(function() { " +
                            "document.querySelector('.row.bvalue').style.display='none'; " +
                            "})()")

                    view.loadUrl("javascript:(function() { " +
                            "document.querySelector('footer').style.display='none'; " +
                            "})()")

                    view.loadUrl("javascript:(function() { " +
                            "document.querySelector('.top-bar-right').style.display='none'; " +
                            "})()")
                };



            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {

                super.onPageStarted(view, url, favicon)
            }

            override fun onReceivedError(
                    view: WebView,
                    request: WebResourceRequest,
                    error: WebResourceError
            ) {

                val errorMessage = "Got Error! $error"

                toastMessage(errorMessage)

                super.onReceivedError(view, request, error)

            }


        }

    }


    override fun onBackPressed() {

        if (webView.canGoBack())
            webView.goBack()

        else
            super.onBackPressed()
    }

    private fun toastMessage(error:String){
        Toast.makeText(this,error, Toast.LENGTH_SHORT).show()
    }



}
